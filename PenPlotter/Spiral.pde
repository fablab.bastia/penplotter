 class SpiralPlot extends SquarePlot
    {
     int svgLineIndex = -1;        // current line within path that is plotting
     
    public String toString()
    {
      return "type:SPIRAL, pixelSize:"+pixelSize+", t1:"+t1Slider.getValue()+
      ", t2:"+t2Slider.getValue()+", t3:"+t3Slider.getValue();
    }
        public void showControls()
        {
            filterDropList.setVisible(true);
            pixelSizeSlider.setVisible(true);
            t1Slider.setVisible(true).setRange(1, 8).setDecimalPrecision(1).setValue(3);
            t2Slider.setVisible(true).setRange(1, 200).setDecimalPrecision(0).setValue(125);
            t3Slider.setVisible(true).setRange(1, 20).setDecimalPrecision(0).setValue(5);
        }

        public void calculate() {
            PImage image = simage;
            float ampScale = t1Slider.getValue();  // amplitude
            float density =  t2Slider.getValue();  // wavelength/density
            int dist = (int) t3Slider.getValue();  // distance between rings
/*
            int maxSize = 768;
            
            if ( image.width > image.height) {
              image.resize (maxSize, 0);
            } else {
              image.resize (0, maxSize);
            }
*/
            
            findPaths(image, ampScale, density, dist);
            drawPreview();
        }

        public boolean addPaths(ArrayList<Path> paths, boolean reverse) {
            if (paths.size() > 0) {
                if (reverse) {
                    for (int i = paths.size() - 1; i >= 0; i--) {
                        paths.get(i).reverse();
                        penPaths.add(paths.get(i));
                    }
                } else {
                    for (Path path : paths) penPaths.add(path);
                }
                return !reverse;
            }
            return reverse;
        }

        public ArrayList<Path> findPaths(PImage image, float ampScale, float density, float dist) {
            boolean up = true;
            float radius = dist/2;                     // Current radius
            float alpha = 0;                           // Initial rotation
            float k;                                   // current radius
            float endRadius;                           // Largest value the spiral needs to cover the image
            float aradius = 1;                             // Radius with brighness applied up
            float bradius = 1;                             // Radius with brighness applied down
            float x, y, xa, ya, xb, yb;                // current X and y + jittered x and y 
            color c;                                   // Sampled color
            float b;                                   // Sampled brightness
            color mask = color (0, 255, 0);        // This color will not be drawn (WHITE)
            Path path = null;
            ArrayList<Path> paths = new ArrayList<Path>();
             penPaths.clear();
  
            k = density/radius ;
            alpha += k;
            radius += dist/(360/k);
            x =  aradius*cos(radians(alpha))+image.width/2;
            y = -aradius*sin(radians(alpha))+image.height/2;
          
            // when have we reached the far corner of the image?
            endRadius = sqrt(pow((image.width/2), 2)+pow((image.height/2), 2));
          
            up = false;
          
            // Have we reached the far corner of the image?
            while (radius < endRadius) {
              k = (density/2)/radius ;
              alpha += k;
              radius += dist/(360/k);
              x =  radius*cos(radians(alpha))+image.width/2;
              y = -radius*sin(radians(alpha))+image.height/2;
          
              // Are we within the the image?
              // If so check if the shape is open. If not, open it
              if ((x>=0) && (x<image.width) && (y>00) && (y<image.height)) {
          
                // Get the color and brightness of the sampled pixel
                c = image.get (int(x), int(y));
                b = brightness(c);
                b = map (b, 0, 255, dist*ampScale, 0);
          
                // Move up according to sampled brightness
                aradius = radius+(b/dist);
                xa =  aradius*cos(radians(alpha))+image.width/2;
                ya = -aradius*sin(radians(alpha))+image.height/2;
          
                // Move down according to sampled brightness
                k = (density/2)/radius ;
                alpha += k;
                radius += dist/(360/k);
                bradius = radius-(b/dist);
                xb =  bradius*cos(radians(alpha))+image.width/2;
                yb = -bradius*sin(radians(alpha))+image.height/2;
          
                // If the sampled color is the mask color do not write to the shape
                if (mask == c) {
                  if (up) {
                    paths.add(path);
                  }
                  up = false;
                } else {
                  // Add vertices to shape
                  if (!up) {
                    path = new Path();
                    up = true;
                  }
                  path.addPoint(xa,ya);
                  path.addPoint(xb,yb);
                }          
              } else {
          
                // We are outside of the image so close the shape if it is open
                if (up) {
                  paths.add(path);
                  up = false;
                }
              }
            }
            addPaths(paths, false);
            return paths;
        }


        public void drawPreview()
        {
            if(preview == null)
            {
              preview = createGraphics(machineWidth,machineHeight);
              preview.beginDraw();
              preview.clear();
              preview.endDraw();
            }
            preview.beginDraw();
            preview.clear();
            preview.strokeWeight(0.1);
  
            preview.stroke(penColor);
            preview.beginShape(LINES);
            for (int i = 0; i < penIndex; i++) {
               Path p = penPaths.get(i);
               for(int j = 0;j<p.size()-1;j++)
               {
                preview.vertex(p.getPoint(j).x, p.getPoint(j).y );
                preview.vertex(p.getPoint(j+1).x, p.getPoint(j+1).y);
               }
            }
            preview.endShape();
            
            preview.stroke(plotColor);
            preview.beginShape(LINES);
            for (int i = penIndex; i < penPaths.size(); i++) {
               Path p = penPaths.get(i);
               for(int j = 0;j<p.size()-1;j++)
               {
                preview.vertex(p.getPoint(j).x, p.getPoint(j).y);
                preview.vertex(p.getPoint(j+1).x, p.getPoint(j+1).y);
               }
            }
            preview.endShape();
            
            preview.endDraw();
            loaded = true;
        }

        public void plot() {
            penIndex = 0;
            svgLineIndex = 0;
            super.plot();
        }
        
        public void nextPlot(boolean preview) {
            if (penIndex < 0) {
                plotting= false;
                plotDone();
                return;
            }

            if (penIndex < penPaths.size()) {
                if (svgLineIndex < penPaths.get(penIndex).size() - 1) {

                    float x1 = penPaths.get(penIndex).getPoint(svgLineIndex).x + homeX - simage.width / 2 + offX;
                    float y1 = penPaths.get(penIndex).getPoint(svgLineIndex).y  + homeY + offY;
                    float x2 = penPaths.get(penIndex).getPoint(svgLineIndex + 1).x + homeX - simage.width / 2 + offX;
                    float y2 = penPaths.get(penIndex).getPoint(svgLineIndex + 1).y  + homeY + offY;


                    if (svgLineIndex == 0) {
                        com.sendPenUp();
                        com.sendMoveG0(x1, y1);
                        com.sendPenDown();
                    }
                    com.sendMoveG1(x2, y2);
                    svgLineIndex++;
                    
                } else {
                    penIndex++;
                    svgLineIndex = 0;
                    nextPlot(true);
                }
            } else // finished
            {
                plotting = false;
                plotDone();
                float x1 = homeX;
                float y1 = homeY;

                com.sendPenUp();
                com.sendMoveG0(x1, y1);
                com.sendMotorOff();
                svgLineIndex = -1;
                penIndex = -1;
            }          
       }

  
    }